//load the libraries
var express = require("express");
var path = require("path");

//create an instance of the app
var app = express();

//master registeration list
var allRegister = [];
var regSubmitList = function(regList){
    return({
        email: regList.email,
        password: regList.password,
        name: regList.name,
        gender: regList.gender,
        birth: regList.birth,
        address: regList.address,
        country: regList.country,
        contact: regList.contact
    });
};

//congifure the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//For registeration submmission --> parameters are going to be in a query string
app.get("/setRegisterList", function(req, res) {
    allRegister.push(regSubmitList(req.query));
	res.status(201).end();
});

//define the route
app.use(express.static(path.join(__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

//start the server
app.listen(app.get("port"), function(){
    console.log("App started at port %d" ,app.get("port"));

});


