//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function() {
    //create an instance of my Angular app
	var RegSub = angular.module("RegSub", []);

    //define the function to be used as controller
	var RegSvc = function($http) {

		var _regSvc = this;

        //service for registeration submit form
		_regSvc.regSubmitList = function(createSubmitObject) {
			return ($http.get("/setRegisterList", {
				params: createSubmitObject
			}));
		};

	};

	//Define the service
	RegSub.service("RegSvc", [ "$http", RegSvc ]);

})();