//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function() {
    //create an instance of my Angular app
    var RegisterApp = angular.module("RegisterApp", [ "RegSub" ]);

    //initialize all value to default for new register
    var initForm = function(_registerCtrl) {
        _registerCtrl.email = "";
        _registerCtrl.password = "";
        _registerCtrl.name = "";
        _registerCtrl.gender = "";
        _registerCtrl.birth = "";
        _registerCtrl.address = "";
        _registerCtrl.country = "";
        _registerCtrl.contact = "";
    }

    //create register object for submit
    var createSubmitObject = function(_registerCtrl) {
		return ({
           email: _registerCtrl.email,
           password: _registerCtrl.password,
           name: _registerCtrl.name,
           gender: _registerCtrl.gender,
           birth: _registerCtrl.birth,
           address: _registerCtrl.address,
           country: _registerCtrl.country,
           contact: _registerCtrl.contact
		});
	}
    
    //define the functions to be used as controller
    var RegisterCtrl = function(RegSvc) {
        var _registerCtrl = this;
        initForm(_registerCtrl);

        //registeration submit controller
        _registerCtrl.registerSubmit = function() {
			RegSvc.regSubmitList(createSubmitObject(_registerCtrl))
				.then(function() {
                    console.log("Information as shown: %s", JSON.stringify(createSubmitObject(_registerCtrl)));
                    initForm(_registerCtrl);
				});
        };

    };
		
    //define the controller
    RegisterApp.controller("RegisterCtrl", [ "RegSvc" , RegisterCtrl ]);

})();